#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bin.downloader import HttpRangeRequest
from bin.parallel import ParallelHttpRangeRequest
from conf.configuration import *
from threading import Thread
import sys

if __name__ == '__main__':
    while True:
        try:
            client = ParallelHttpRangeRequest(conf=CONFIG)
            client.cycle()
        except ConnectionResetError:
            print("Connection reset")
            sys.exit(0)

        except ConnectionRefusedError:
            print('Connection Refused')
            sys.exit(0)

        except KeyboardInterrupt:
            print("Quit")
            sys.exit(0)
