# -*- coding: utf-8 -*-

from bin.downloader import *
from threading import Thread, activeCount
from collections import deque
from websocket import create_connection
import json
import time
import sys

def write_data_to_file(hrr: HttpRangeRequest):

    '''
    write data as binary code
    '''

    if hrr.is_document_downloaded:
        return None
    type = 'ab' if hrr.is_file_exist else 'xb'
    with open(hrr.file_name, type) as f:
        f.write(hrr.content)

class ParallelHttpRangeRequest():

    def __init__(self, conf: dict):

        self.conf = conf

        self.ws = create_connection(conf['WEBSOCKETSERVER'])

    def cycle(self):

        '''
        Entry point to multithreading client
        '''

        while True:
            # checking count of threads
            if activeCount() >= self.conf['NUMBER_OF_THREADS']:
                time.sleep(self.conf['SLEEP'])
                continue

            # getting url and params for range request from websocket server
            self.ws.send(json.dumps({'get_url': True}))
            data = self.ws.recv()

            # if websocket server doesn't have any url then data equals 'all'
            if data == 'all':
                time.sleep(self.conf["SLEEP"])
                continue
            params = json.loads(data)

            # compose thread for request
            Thread(target=self.run_range_request, args=(params,)).start()

    def run_range_request(self, params):

        '''
        Thread for range request
        '''

        # send HEAD http request
        hrr = HttpRangeRequest(params['full_url'], conf=self.conf)

        # set needed params
        hrr.start_point = params['start_point']
        hrr.is_file_exist = params['is_file_exist']
        hrr.file_name = params['file_name']

        # send GET http request
        if params['is_range_request']:
            try:
                hrr.get()
                hrr.is_document_downloaded = False
            except StopSendingRangeRequestsError:
                hrr.is_document_downloaded = True
        else:
            hrr.get_full()
            hrr.is_document_downloaded = True

        write_data_to_file(hrr)

        # send data about request to websocket sever
        self.ws.send(json.dumps({
            'get_url': False,
            'full_url': hrr.full_url,
            'is_range_request': hrr.is_range_request,
            'start_point': hrr.start_point,
            'file_name': hrr.file_name,
            'is_document_downloaded': hrr.is_document_downloaded
        }))
        self.ws.recv()

        sys.exit('Thread is closed')
