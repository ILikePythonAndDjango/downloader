# -*- coding: utf-8 -*-

from tornado.websocket import WebSocketHandler
from bin.downloader import HttpRangeRequest
from collections import deque
import json

def handle_websocket_message(on_message):

    '''
    veiw range request process
    '''

    def print_params(tornado_instance, message):
        if not message.get('get_url', False):
            print('\n')
            if message.get('is_document_downloaded', False):
                print('Document from {url} is downloaded: \n{file_protocol}\n'.format(
                    url=message['full_url'],
                    file_protocol='file://' + message['file_name']
                ))
            else:
                print('number_of_requests_in_queue: {number}\nrange: {range}\n\nurl: {url}\nfile_name: {file_name}\nprogress: {bytes} bytes\nrange_request: {is_range_request}\n'.format(
                    number=len(tornado_instance.range_requests),
                    range=tornado_instance.conf['RANGE'],
                    url=message['full_url'],
                    file_name=message['file_name'],
                    bytes=message['start_point'],
                    is_range_request="allowed" if message['is_range_request'] else 'not allowed'
                ))
        on_message(tornado_instance, message)

    return print_params

def data_json_to_pyobject(event):

    '''
    convert JSON to python's objects
    '''

    def tornado_websocket_method(tornado_instance, message):
        message = json.loads(message)
        event(tornado_instance, message)

    return tornado_websocket_method


class EchoWebSocket(WebSocketHandler):

    def __init__(self, conf: dict, application, request, **kwargs):
        self.conf = conf
        super(EchoWebSocket, self).__init__(application, request, **kwargs)

    def open(self):
        self.range_requests = deque([HttpRangeRequest(url, conf=self.conf) for url in self.conf['URLS']])
        print('Start!')

    @data_json_to_pyobject
    @handle_websocket_message
    def on_message(self, message):
        data = self.compose_response(message)
        self.write_message(data)

    def compose_response(self, params):
        # start range request
        if params['get_url']:
            if not len(self.range_requests):
                return 'all'
            hrr = self.range_requests.popleft()
            return {
                'is_range_request': hrr.is_range_request,
                'start_point': hrr.start_point,
                'file_name': hrr.file_name,
                'full_url': hrr.full_url,
                'is_file_exist': hrr.is_file_exist,
            }

        # continue range request
        else:
            if params['is_document_downloaded']:
                return {'ok': True}
            hrr = HttpRangeRequest(params['full_url'], conf=self.conf)
            hrr.start_point = params['start_point']
            hrr.file_name = params['file_name']
            hrr.is_file_exist = True
            self.range_requests.append(hrr)
            return {'ok': True}

    def on_close(self):
        print('End!')
