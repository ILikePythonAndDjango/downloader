#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import sys
import os.path
from string import digits, ascii_letters, ascii_lowercase, ascii_uppercase
from random import choice as c


symbols = digits + ascii_letters + ascii_lowercase + ascii_uppercase

def generate_random_string(length=20):
    return ''.join([c(symbols) for counter in range(length)])

class RangeRequestIsntSuportedError(Exception):

    '''
    For handling requests that don't suporte Range request
    '''

    pass

class Http416Error(Exception):

    '''
    For handling http code 416
    '''

    pass

class Http404Error(Exception):

    '''
    For handling http code 404
    '''

    pass

class StopSendingRangeRequestsError(Exception):

    '''
    You should raise this exception when you've downloaded ful document
    '''

    pass

class HttpRangeRequest():

    def __init__(self, full_url: str, conf: dict):
        '''
        Set data and checking host have opportunity range request
        '''

        # Is it range requests
        head_response = requests.head(full_url)
        if not head_response.headers.get('Accept-Ranges', 'none') == 'none':
            self.is_range_request = True
        else:
            self.is_range_request = False

        if head_response.status_code == requests.codes.NOT_FOUND:
            raise Http404Error

        self.full_url = full_url
        self.url = head_response.url
        self.headers = head_response.headers
        self.full_content_length = int(head_response.headers.get('Content-Length', 0))
        self.MIME_type = head_response.headers.get('Content-Type', 'text/plain').split('/')
        self.status_code = head_response.status_code

        # range_bute is diffrence between start point and end point
        self._range = int(conf.get('RANGE', 1024 * 5))
        self._start_point = int(conf.get('START_POINT', 0))

        self.content = u''
        self.text = ''

        # Is True if docoment is downloaded
        self.is_document_downloaded = False

        # set True if you try sent request without range
        self.is_request_without_range = False

        #set file's name
        self.file_name = '{dir}/{name}{range}.{type}'.format(
            dir=os.path.join(conf['BASE_DIR'], conf['DOWNLOAD_DIR']),
            name=generate_random_string(),
            url=self.full_url,
            type=self.MIME_type[1],
            range='' if self.is_range_request else '_full'
        )

        self.is_file_exist = False

        if self.full_content_length < self._range:
            self._range = self.full_content_length

    def __bool__(self):
        return self.is_range_request

    def __repr__(self):
        return "<HttpRangeRequest '{url}' status_code='{code}' range_request='{is_range}' range_byte='{range_byte}'>".format(
            url=self.full_url,
            is_range=('allowed' if self.is_range_request else 'not allowed'),
            range_byte=self._range,
            code=self.status_code
        )

    def __str__(self):
        return self.full_url

    @property
    def start_point(self):
        return self._start_point

    @start_point.setter
    def start_point(self, sp):
        self._start_point = sp

    def increase_start_point(self):
        self._start_point += self._range

    def get(self):

        if self.full_content_length <= self._start_point:
            self.is_document_downloaded = True
            raise StopSendingRangeRequestsError

        #indicate that server not allowed range request
        if not self.is_range_request:
            raise RangeRequestIsntSuportedError

        if (self.full_content_length - self._start_point) < self._range:
            self._range = self.full_content_length - self._start_point

        #send request
        headers_for_new_range_request = {
            "Range": "bytes={}-{}".format(self._start_point, self._start_point + self._range - 1)
        }
        range_response = requests.get(self.full_url, headers=headers_for_new_range_request)

        if range_response.status_code == requests.codes.REQUESTED_RANGE_NOT_SATISFIABLE:
            raise Http416Error

        self.text = range_response.text
        self.content = range_response.content

        #reset headers and propertis
        self.headers = range_response.headers
        self.status_code = range_response.status_code
        self.increase_start_point()

        return self

    def get_full(self):

        '''
        GET request without range request
        '''

        if self.is_request_without_range:
            return self

        response = requests.get(self.full_url)

        self.status_code = response.status_code
        self.headers = response.headers

        self.text = response.text
        self.content = response.content

        self.is_request_without_range = True

        return self
