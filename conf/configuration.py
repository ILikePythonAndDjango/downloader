#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os.path

__all__ = ["CONFIG"]

def get_urls(conf):
    file_with_urls = os.path.join(os.path.join(conf["BASE_DIR"], conf["DOWNLOAD_DIR"]), 'urls')
    with open(file_with_urls) as f:
        return f.read().strip().split('\n')

CONFIG = {
    'RANGE': 1024 * 200,
    'NUMBER_OF_THREADS': 12,
    'START_POINT': 0,
    'BASE_DIR': os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'DOWNLOAD_DIR': 'downloads',
    'WEBSOCKETSERVER': 'ws://localhost:10000/',
    'PORT': 10000,
    'SLEEP': 2.0,
}
CONFIG['URLS'] = get_urls(CONFIG)
