# downloader

## Stack

Python==3.6.5

requests==2.19.1

Tornado==5.1

websocket-client==0.51.0

## Usage

If you want to run then firstly execute `./runserver.py` after in another terminal `./runclient.py`

### multithreading Http client

If you try to run `./runclient.py` you don't see information about requests. This script only works in the background. It downloads and save files that the websocket server gives back.
I used chaining. Chaining is design pattern. When you try to run `HttpRangeRequest.get()` it returns instance of `HttpRangeRequest` with new properties.
`HttpRangeRequest.content` contains response body as binary code. If you try to initilize `HttpRangeRequest` it do HTTP `HEAD` request and set properties.

### WebSocket Server written on Tornado

If you want current url then you must sent request to `ws://localhost/:10000` or on other port. 
You can see information about process within terminal.

```
Start!


number_of_requests_in_queue: 0
range: 204800

url: https://i.imgur.com/RuzsRbF.jpg
file_name: /home/nikita/downloader/downloads/NzU85rSBlA5dm6f0dWSD.jpeg
progress: 55828 bytes
range_request: allowed



Document from https://i.imgur.com/RuzsRbF.jpg is downloaded: 
file:///home/nikita/downloader/downloads/NzU85rSBlA5dm6f0dWSD.jpeg



number_of_requests_in_queue: 0
range: 204800

url: https://i.imgur.com/T0qq8bz.jpg
file_name: /home/nikita/downloader/downloads/COkIpbWGmtveGbeduBcF.jpeg
progress: 130570 bytes
range_request: allowed



number_of_requests_in_queue: 1
range: 204800

url: http://i.imgur.com/z4d4kWk.jpg
file_name: /home/nikita/downloader/downloads/eC0nKPKWDS35WpJCGAsc.jpeg
progress: 146515 bytes
range_request: allowed

...

url: http://www.kinyu-z.net/data/wallpapers/38/834414.jpg
file_name: /home/nikita/downloader/downloads/wCthX7xa6agJwMCDCPJQ.jpeg
progress: 849851 bytes
range_request: allowed



Document from http://www.kinyu-z.net/data/wallpapers/38/834414.jpg is downloaded: 
file:///home/nikita/downloader/downloads/wCthX7xa6agJwMCDCPJQ.jpeg

```

### How to put urls

Directory `downloads` contains `urls`. It's file that have url on every line. If you put empty line between two urls then websocket server will not run.

```
https://i.imgur.com/T0qq8bz.jpg
http://i.imgur.com/z4d4kWk.jpg
http://www.kinyu-z.net/data/wallpapers/38/834414.jpg
http://www.kinyu-z.net/data/wallpapers/136/1179015.jpg
http://www.intrawallpaper.com/static/images/HD-Wallpapers1_llg6zA4.jpeg
https://i.imgur.com/RuzsRbF.jpg
https://cdn.keycdn.com/img/cdn-stats.png
```

### How to put configuration file

Configuration file is just simple python script that contains dictionary that called `CONFIG`. You must put it to `HttpRangeRequest`, `ParallelHttpRangeRequest`, `EchoWebSocket`.

```
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os.path

__all__ = ["CONFIG"]

def get_urls(conf):
    file_with_urls = os.path.join(os.path.join(conf["BASE_DIR"], conf["DOWNLOAD_DIR"]), 'urls')
    with open(file_with_urls) as f:
        return f.read().strip().split('\n')

CONFIG = {
    'RANGE': 1024 * 200,
    'NUMBER_OF_THREADS': 12,
    'START_POINT': 0,
    'BASE_DIR': os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    'DOWNLOAD_DIR': 'downloads',
    'WEBSOCKETSERVER': 'ws://localhost:10000/',
    'PORT': 10000,
    'SLEEP': 2.0,
}
CONFIG['URLS'] = get_urls(CONFIG)
```
Properties that listed above must be in configuration. 