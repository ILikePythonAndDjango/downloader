#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tornado.web import Application
from tornado.ioloop import IOLoop
from bin.websocketserver import EchoWebSocket
from conf.configuration import *
from functools import partialmethod
import sys

def partialclass(cls, *args, **kwargs):

    class PartialClass(cls):

        __init__ = partialmethod(cls.__init__, *args, *kwargs)

    return PartialClass

def make_app():
    return Application([
        (r"/", partialclass(EchoWebSocket, CONFIG)),
    ])

if __name__ == '__main__':
    app = make_app()
    app.listen(CONFIG['PORT'])
    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        print("Quit")
        sys.exit(0)
